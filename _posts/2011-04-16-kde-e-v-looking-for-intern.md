---
title: 'KDE e.V. looking for Intern'
date: 2011-04-16 00:00:00 
layout: post
---

KDE e.V., the non&#173;profit organisation supporting the KDE community, is looking for a full-time intern for its Berlin office at the earliest possible date.

If you are looking for an intern position for 3-6 months in an international non-profit organization, and want to help to organize the Berlin Desktop Summit, take care of the supporting membership program and work on promotional material, please send us your application.

The position is an internship under regular German conditions. More details about the internship and how to apply can be found <a href="http://ev.kde.org/resources/internship-advert_EN-updated.pdf">here</a>.
      
