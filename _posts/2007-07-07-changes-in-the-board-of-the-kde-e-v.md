---
title: 'Changes in the board of the KDE e.V.'
date: 2007-07-07 00:00:00 
layout: post
---

<p>After serving in the board for five years Eva Brucherseifer decided
        to focus on other endeavors. At the General Assembly 2007 held in
        Glasgow during aKademy Klaas Freitag was elected as her successor. The
        other members of the board are Aaron Seigo who takes over the role of
        president from Eva, Cornelius Schumacher who remains Vice President and
        Treasurer, Adriaan de Groot as second Vice President and Sebastian
        K&#252;gler as additional board member.</p>

        <p>Thanks, Eva, for all the work you did for the e.V. in the last five
        years, and welcome, Klaas, to the new board.</p>
      